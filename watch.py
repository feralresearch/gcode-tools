import os
import sys
import time

from watchdog.observers import Observer
from watchdog.events import RegexMatchingEventHandler

from convert import convert


class EventHandler(RegexMatchingEventHandler):
    IMAGES_REGEX = [r".*\.svg$"]

    def __init__(self):
        super().__init__(self.IMAGES_REGEX)

    def on_created(self, event):
        self.process(event)

    def on_modified(self, event):
        self.process(event)

    def on_deleted(self, event):
        filename, ext = os.path.splitext(event.src_path)
        gcode_file = f"{filename}.gcode"
        print(gcode_file)
        try:
            print(f"DELETE: {gcode_file}")
            os.remove(gcode_file)
        except:
            print(f"DELETE: ERROR: {gcode_file}")
            print(f"DELETE: ERROR:", sys.exc_info()[0])

    def process(self, event):
        convert(event.src_path)


class FileWatcher:
    def __init__(self, src_path):
        self.__src_path = src_path
        self.__event_handler = EventHandler()
        self.__event_observer = Observer()

    def run(self):
        self.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.stop()

    def start(self):
        self.__schedule()
        self.__event_observer.start()

    def stop(self):
        self.__event_observer.stop()
        self.__event_observer.join()

    def __schedule(self):
        self.__event_observer.schedule(
            self.__event_handler,
            self.__src_path,
            recursive=True
        )


if __name__ == "__main__":
    src_path = sys.argv[1] if len(sys.argv) > 1 else '.'
    print(f"WATCHING: {src_path}")
    FileWatcher(src_path).run()
